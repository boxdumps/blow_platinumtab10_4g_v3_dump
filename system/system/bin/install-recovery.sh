#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/recovery:41943040:ed504af1706babb36235f1680056c00234375e6c; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/boot:36700160:6c211524484d9df43ff4d33ca6faef07f7f2b647 \
          --target EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/recovery:41943040:ed504af1706babb36235f1680056c00234375e6c && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
