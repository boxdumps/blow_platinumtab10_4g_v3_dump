## android10-user 10 QP1A.190711.020 20200810 release-keys
- Manufacturer: incar
- Platform: sp9863a
- Codename: PlatinumTAB10_4G_V3
- Brand: BLOW
- Flavor: android10-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: 21112
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: pl-PL
- Screen Density: undefined
- Fingerprint: BLOW/PlatinumTAB10_4G_V3/PlatinumTAB10_4G_V3:10/QP1A.190711.020/21112:user/release-keys
- OTA version: 
- Branch: android10-user-10-QP1A.190711.020-20200810-release-keys
- Repo: blow_platinumtab10_4g_v3_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
